#!/usr/bin/env python3
from sys import argv

PROJ_PREFIX = "c proj"
TARGET_PREFIX = "c target"
IND_PREFIX = "c ind"
PROBLEM_PREFIX = "p cnf"

def clause_to_bytes(clause):
    """
    Turns a clause into a clause line
    """
    return ("{} 0").format(" ".join(map(str, clause)))

def mk_cnf(nbr, clauses, comments=[], ind=[]):
    return comments + [(f"p cnf {nbr} {len(clauses)}")] + list(map(clause_to_bytes, clauses))




def self_product(nbr, clauses, target, proj, amount=1):
    new_proj = proj.copy()
    new_clauses = clauses.copy()

    varmap = dict()
    def get_varmapped(var, index, nbr, target):
        if var in target:
            return var

        if (var, index) in varmap:
            return varmap[(var, index)]

        nvar = nbr + len(varmap) + 1 # Because variables are 1-indexed
        varmap[(var, index)] = nvar
        return nvar

    for j in range(amount):
        # Add the new clauses
        for c in clauses:
            nc = set()
            for lit in c:
                if abs(lit) in target:
                    nc.add(lit)
                else:
                    nvar = get_varmapped(abs(lit), j, nbr, target)
                    nc.add((1 if lit > 0 else -1) * nvar)
                    if abs(lit) in proj:
                        new_proj.add(nvar)
            new_clauses.add(frozenset(nc))

    return nbr + len(varmap), new_proj, new_clauses

def cnf_line_to_clause(line: str):
    return frozenset(map(int, filter(lambda i: len(i) > 0 and i != "0",
                                line.strip().split(" "))))


def read_cnf(fname: str):
    nbr = -1
    proj = set()
    target = set()
    res = set()
    with open(fname, 'r') as file:
        for line in file:
            if line.startswith(TARGET_PREFIX):
                target.update(set(cnf_line_to_clause(line[len(TARGET_PREFIX):])))
            if line.startswith(PROJ_PREFIX):
                proj.update(set(cnf_line_to_clause(line[len(PROJ_PREFIX):])))
            elif line.startswith(PROBLEM_PREFIX):
                nbr = int(line.split(" ")[-2])
            elif not (line.startswith("p") or line.startswith("c")):
                s = cnf_line_to_clause(line)
                if len(s) > 0:
                    res.add(s)
    return (nbr, proj, target, res)

nbr, proj, target, clauses = read_cnf(argv[-1])

k = int(argv[1])

new_nbr, new_proj, new_clauses = self_product(nbr, clauses, target, proj, amount=k)

print(TARGET_PREFIX, *target, 0)
print(PROJ_PREFIX, *new_proj, 0)
print(PROBLEM_PREFIX, new_nbr, len(new_clauses))
for clause in new_clauses:
    print(*clause, 0)
