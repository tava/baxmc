#!/usr/bin/env python

from os import path,walk
from sys import argv
from collections import defaultdict

if len(argv) < 2:
    print(f"Usage: {argv[0]} benchmark paths...")

def remove_all_exts(current):
    # Not really optimized...
    attempt = path.splitext(current)[0]
    while attempt != current:
        current = attempt
        attempt = path.splitext(attempt)[0]

    return attempt

# The file format for benchmarks output is the following:
# If the benchmark terminated:
#    ...
#    v count
#    s solution
#    000.0user
#    one line of junk
# Otherwise:
#    No v line

def headers(dirs):
    for d in dirs:
        dname = path.dirname(d)
        yield dname + " time"
        yield dname + " count"

print("benchmark", *headers(argv[1:]), sep=',')

benchs = defaultdict(lambda: [])

for (i, dir) in enumerate(argv[1:]):
    acc = []
    for (dirpath, dirnames, filenames) in walk(dir):
        d = dirpath.removeprefix(dir)
        acc += [path.join(d, file) for file in filenames]

    acc.sort()

    # now read each file
    for fname in acc:
        key = remove_all_exts(fname)
        with open(path.join(dir, fname)) as file:
            lines = file.readlines()

            count_line = lines[-4]
            if count_line.startswith("v "):
                count = float(count_line.removeprefix("v ").strip())
                time = float(lines[-2].split()[0].removesuffix("user"))
                # This is a count line so the thing finished
                benchs[key].append((time, count))
            else:
                benchs[key].append((7200., 0.))


    for (bname, lst) in benchs.items():
        if lst == i:
            # We did not run this benchmark ?
            raise RuntimeError(f"Missing benchmark for {dir}")

for (bname, lst) in sorted(benchs.items(), key=lambda t: t[1][0]):
    print(bname, *(f"{time},{count}" for (time, count) in lst), sep=",")
