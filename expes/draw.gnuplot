#!/bin/gnuplot -p
set terminal png size 2160,1080
set output 'compare.png'
set key top left
set logscale y 2
set ylabel "Time"

# Copied from https://www.xmodulo.com/plot-bar-graph-gnuplot.html
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics rotate by 45 right
set xtics format ""
set grid ytics

set datafile separator ','
set key autotitle columnhead
stats filename u 0 nooutput
plot for [i=2:STATS_columns:2] filename using i:xtic(1)
