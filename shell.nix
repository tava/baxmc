{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell
{
  buildInputs = ((import ./deps.nix) { }) ++ [pkgs.cmake pkgs.python3 pkgs.hyperfine pkgs.time
  pkgs.util-linux pkgs.gtest pkgs.skopeo];
}
