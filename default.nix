{ pkgs ? import <nixpkgs> { } }:
let
  curdirSource = if (builtins.getEnv "CI" == "true") then ./. else fetchGit ./.;
  deps = ((import ./deps.nix) { });
  baxmc = pkgs.stdenv.mkDerivation {
    name = "baxmc";
    src = curdirSource;
    nativeBuildInputs = [ pkgs.cmake ];
    buildInputs = deps;
    cmakeFlags = [ "-DWITH_TESTS=OFF" ];
  };
  benchmarks = pkgs.stdenv.mkDerivation {
    name = "benchmarks";
    src = curdirSource;
    nativeBuildInputs = [
      pkgs.coreutils
      pkgs.gnutar
    ];
    dontConfigure = true;
    dontPatch = true;
    buildPhase = ''
      tar -czv $(find benchmarks/ -type f -name "*.dimacs" -or -name "*.cnf") -f benchmarks.tar.gz
    '';
    installPhase = ''
      mkdir -p $out/
      cp README-DOCKER.md $out/
      cp maxcount.py $out/
      cp benchmarks.tar.gz $out/
    '';
  };
in
pkgs.dockerTools.buildLayeredImage
{
  name = "baxmc";
  tag = "latest";
  created = "now";
  contents = [
    pkgs.gzip
    pkgs.gnutar
    pkgs.fish
    pkgs.coreutils
    pkgs.python3
    baxmc
    benchmarks
  ] ++ deps;
  config.Cmd = [ "${pkgs.fish}/bin/fish" ];
}
