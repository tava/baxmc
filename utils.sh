#!/bin/sh

run_cmd() {
  logfile="$1"
  shift 1
  time -o "$logfile" -a -- prlimit --as=10737418240 timeout -s 9 2h "$@" | tee "$logfile"
}
