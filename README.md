# BaxMC

There is mainly three ways of using `baxmc`:
1. Using docker
2. Using the provided nix flake
3. By building from source

In all of the three cases, after appropriate command have been issued,
you can use `baxmc` and get help using:
```bash
baxmc --help
```

## Using docker

Assuming you have docker installed:
```bash
docker run -it gricad-registry.univ-grenoble-alpes.fr/tava/baxmc:latest
```

This docker image is updated on CI, so that only the latest
version is available. You can check [here](https://gricad-gitlab.univ-grenoble-alpes.fr/tava/baxmc/-/environments/108) what version of the
code is actually deployed in the docker image.

## Using `nix`

Assuming you have nix installed, and flakes are enabled:
```bash
nix shell git+https://gricad-gitlab.univ-grenoble-alpes.fr/tava/baxmc.git#baxmc
```

This is more flexible and allows you to specify the revision:
```bash
nix shell git+https://gricad-gitlab.univ-grenoble-alpes.fr/tava/baxmc.git?rev=$MYREVISION#baxmc
```

## Building

Using `nix` is prefered:
```
nix-shell --pure

mkdir build && cd build && cmake .. && make

./baxmc -h
```

## License

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 
