#!/bin/gnuplot -p
set terminal png size 1080,540
set output 'out.png'
set logscale y 2
set key bottom right

set datafile separator ','
set key autotitle columnhead
plot 'out.csv' using 1:2 with lines, 'out.csv' using 1:3 with lines, 'out.csv' using 1:4 with lines
