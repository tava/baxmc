{
  description = "BaxMC: a Max#SAT solver";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          system = system;
          overlays = [ (import ./overlay.nix) ];
        };
      in
      {
        packages = rec {
          baxmc = pkgs.stdenv.mkDerivation {
            name = "baxmc";
            src = self;
            buildInputs = ((import ./deps.nix) { pkgs = pkgs; });
            nativeBuildInputs = [ pkgs.cmake ];
            cmakeFlags = [ "-DWITH_TESTS=OFF" ];
          };
          default = baxmc;
        };
      });
}
