{ pkgs ? import <nixpkgs> {
    overlays = [ (import ./overlay.nix) ];
  }
}:
let
  unigen = (
    with pkgs;
    stdenv.mkDerivation {
      name = "unigen";
      enableParallelBuilding = true;
      src = fetchFromGitHub {
        owner = "meelgroup";
        repo = "unigen";
        rev = "583c0e7d1bf39da8bfb600ae44ef5e6057acde93";
        sha256 = "11skxw014zj560pwx9rsbgik921cbsk928kklbna7qjzqvrk8bc7";
        fetchSubmodules = true;
      };

      buildInputs = [
        pkgs.gmp
        pkgs.zlib
        pkgs.boost
        pkgs.cryptominisat
        approxmc
      ];

      nativeBuildInputs = [
        pkgs.cmake
        pkgs.pkg-config
      ];
    }
  );
  approxmc = (
    with pkgs;
    pkgs.stdenv.mkDerivation {
      name = "approxmc";
      enableParallelBuilding = true;
      src = fetchFromGitHub {
        owner = "meelgroup";
        repo = "approxmc";
        rev = "a3e108f269fd0f3541e95470b44cfdf1946c7966";
        sha256 = "1h7rxwn8hdffz8c8gr1m7cvmg5pla18wv2ylgjlqzckfmlpysyla";
      };
      buildInputs = [
        pkgs.gmp
        pkgs.zlib
        pkgs.boost
        pkgs.cryptominisat
      ];

      nativeBuildInputs = [
        pkgs.cmake
        pkgs.pkg-config
      ];
    }
  );
in
[
  unigen
  approxmc
  pkgs.cryptominisat
  pkgs.boost
  pkgs.bliss
]
