/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include "bliss/graph.hh"
#include <approxmc/approxmc.h>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <iterator>
#include <ostream>
#include <problem.hpp>
#include <set>
#include <string>
#include <unistd.h>
#include <unordered_set>
#include <vector>

#define STARTSWITH(str, start) ((line).rfind((start), 0) == 0)

using namespace BaxMC;
using namespace CMSat;

using std::map;
using std::set;
using std::string;
using std::unordered_set;

Clause::Clause(unordered_set<Lit> inner) : inner(inner) {}

bool Clause::operator==(const Clause &other) const {
  return other.inner == inner;
}

bool Clause::contains(const Lit &l) const {
  return this->inner.find(l) != this->inner.end();
}

bool Clause::contains(const Var &v) const {
  // XXX: We need to do that weird construction otherwise it complains
  Lit neg = Lit(v, true);
  Lit pos = Lit(v, true);
  return this->contains(neg) || this->contains(pos);
}

void read_variable_set(std::string line, unordered_set<Var> &destination) {
  size_t prev = 0;
  size_t curpos = 0;
  assert(line.back() == '0');
  while ((curpos = line.find(" ", prev)) != std::string::npos) {
    std::string tmp = line.substr(prev, curpos - prev);
    assert(tmp[0] != '-');
    if (tmp[0] == '0') {
      break;
    }
    uint32_t varnum = std::stoi(tmp);
    destination.insert(varnum - 1);

    // Past the last ' '
    prev = curpos + 1;
  }
}

unordered_set<Lit> cnf_line_to_clause(string line) {
  size_t prev = 0;
  size_t curpos = 0;
  unordered_set<Lit> accumulator;
  assert(line.back() == '0');

  while ((curpos = line.find(" ", prev)) != std::string::npos) {
    string tmp = line.substr(prev, curpos - prev);
    bool is_inverted = false;
    if (tmp[0] == '-') {
      is_inverted = true;
      tmp = tmp.substr(1, tmp.size());
    } else if (tmp[0] == '0') {
      break;
    }
    uint32_t varnum = std::stoi(tmp);
    accumulator.insert(Lit(varnum - 1, is_inverted));

    // Past the last ' '
    prev = curpos + 1;
  }

  return accumulator;
}

Problem::Problem(string fname) : Problem(std::ifstream(fname)) {}

Problem::Problem(std::ifstream infile) : nVars(0) {
  std::string line;

  if (infile.is_open()) {
    while (infile) {
      std::getline(infile, line);
      if (!line.size()) {
        continue;
      }
      if (STARTSWITH(line, "c target")) {
        // This is the target line
        read_variable_set(line.substr(9), target);
      } else if (STARTSWITH(line, "c proj")) {
        // This is the projection line
        read_variable_set(line.substr(7), proj);
      } else if (line[0] == 'p') {
        // The problem line
        // Only get the number of variables here, as we will get the other
        // information later

        // Start the search after "p cnf"
        size_t end_nvars = line.find(' ', 6);
        assert(end_nvars < std::string::npos);
        this->nVars = std::stoi(line.substr(6, end_nvars - 6));

        size_t start_nclauses = line.rfind(' ');
        assert(start_nclauses < std::string::npos);
        size_t nclauses = std::stoi(line.substr(start_nclauses));
        this->clauses.reserve(nclauses);
      } else if (line[0] == 'c') {
        // Ignore this comment line
        continue;
      } else {
        assert(nVars > 0);
        // This is a clause line
        add_clause(cnf_line_to_clause(line));
      }
    }
  }
};

struct automorphism_data {
  map<unsigned int, Lit> *vertex_map;
  Problem *prob;
  size_t symetries;
  size_t verbosity;
};

void automorphism_callback(void *up, unsigned int size,
                           const unsigned int *aut) {
  struct automorphism_data *data = (automorphism_data *)up;
  map<unsigned int, Lit> vmap = *(data->vertex_map);
  Problem *prob = data->prob;

  map<Lit, Lit> mapping;
  set<Lit> support;
  for (size_t i = 0; i < size; i++) {
    if (i != aut[i] && vmap.find(i) != vmap.end()) {
      // Maybe in the mapping
      Lit source = vmap[i];
      Lit dest = vmap[aut[i]];
      if (!prob->is_target_var(source.var())) {
        // Can't do that here
        return;
      }
      support.insert(source);
      support.insert(dest);
      mapping[source] = dest;
    }
  }

  data->symetries++;
  Permutation *p = Permutation::create(mapping);
  set<Var> supp = p->support();
  if (data->verbosity >= 2) {
    printf("c Processed permutation %lu with support", p->get_id());
    for (auto v = supp.begin(); v != supp.end(); v++) {
      printf(" %u", *v);
    }
    printf("\n");
  }
  prob->perm_ids.push_back(p->get_id());
}

void Problem::analyze_symetries(size_t verbosity) {
  bliss::Stats s;
  bliss::Graph graph;
  map<Lit, unsigned int> literal_map;
  map<unsigned int, Lit> vertex_map;

  // First create literal nodes, with different color for projection variables
  for (size_t var = 0; var < nVars; var++) {
    Lit pos_var = Lit(var, false);
    Lit neg_var = Lit(var, true);

    unsigned int color = is_target_var(var) ? 2 : 1;

    literal_map[pos_var] = graph.add_vertex(color);
    literal_map[neg_var] = graph.add_vertex(color);
    vertex_map[literal_map[pos_var]] = pos_var;
    vertex_map[literal_map[neg_var]] = neg_var;

    // Boolean consistency edge
    graph.add_edge(literal_map[pos_var], literal_map[neg_var]);
  }

  for (auto &clause : clauses) {
    unsigned int clause_vertex = graph.add_vertex(0);
    for (auto &lit : clause) {
      graph.add_edge(clause_vertex, literal_map[lit]);
    }
  }

  struct automorphism_data data = {.vertex_map = &vertex_map,
                                   .prob = this,
                                   .symetries = 0,
                                   .verbosity = verbosity};

  graph.find_automorphisms(s, automorphism_callback, &data);

  if (data.symetries > 0) {
    printf("c Problem has %lu symetries\n", data.symetries);
  } else {
    printf("c No symetry detected.\n");
  }
}

void Problem::add_clause(Clause clause) { clauses.insert(clause); }

bool Problem::is_projection_var(Var var) {
  return (proj.find(var) != proj.end());
}

bool Problem::is_target_var(Var var) {
  return (target.find(var) != target.end());
}

bool is_clause_inactivated(Clause clause, Clause assumps) {
  for (auto &lit : clause) {
    if (assumps.contains(lit)) {
      return true;
    }
  }

  return false;
}

bool Problem::dont_care(Var v, Clause assumps) {
  for (auto &clause : clauses) {
    if (is_clause_inactivated(clause, assumps)) {
      continue;
    }
    for (auto &lit : clause) {
      if (lit.var() == v) {
        return false;
      }
    }
  }
  return true;
}

void Problem::show_restricted_graph(const char *const fname, Clause assumps) {
  bliss::Graph graph;
  map<Lit, unsigned int> literal_map;
  map<unsigned int, Lit> vertex_map;

  // First create literal nodes, with different color for projection variables
  for (Var var = 0; var < nVars; var++) {

    if (assumps.contains(var)) {
      continue;
    }
    Lit pos_var = Lit(var, false);
    Lit neg_var = Lit(var, true);

    unsigned int color = is_target_var(var) ? 2 : 1;

    literal_map[pos_var] = graph.add_vertex(color);
    literal_map[neg_var] = graph.add_vertex(color);

    if (is_target_var(var)) {
      std::cout << "Max Literal " << pos_var << " " << literal_map[pos_var]
                << std::endl;
      std::cout << "Max Literal " << neg_var << " " << literal_map[neg_var]
                << std::endl;
    } else if (is_projection_var(var)) {
      std::cout << "Proj Literal " << pos_var << " " << literal_map[pos_var]
                << std::endl;
      std::cout << "Proj Literal " << neg_var << " " << literal_map[neg_var]
                << std::endl;
    }
    vertex_map[literal_map[pos_var]] = pos_var;
    vertex_map[literal_map[neg_var]] = neg_var;

    // Boolean consistency edge
    graph.add_edge(literal_map[pos_var], literal_map[neg_var]);
  }

  for (auto clause = clauses.begin(); clause != clauses.end(); clause++) {
    if (is_clause_inactivated(*clause, assumps)) {
      continue;
    }
    unsigned int clause_vertex = graph.add_vertex(0);
    for (auto lit = clause->begin(); lit != clause->end(); lit++) {
      graph.add_edge(clause_vertex, literal_map[*lit]);
    }
  }

  graph.write_dot(fname);
}
