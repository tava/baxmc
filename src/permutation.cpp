/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include "permutation.hpp"
#include <algorithm>
#include <cstdint>

using namespace BaxMC;
using namespace CMSat;

using std::map;
using std::set;
using std::vector;

size_t Permutation::next_id = 0;
vector<Permutation *> Permutation::all_perms;

Permutation *Permutation::create(std::map<CMSat::Lit, CMSat::Lit> mapping) {
  Permutation *p = new Permutation(mapping);
  all_perms.push_back(p);
  assert(all_perms[p->get_id()] == p);
  return p;
}

Permutation::Permutation(map<Lit, Lit> mapping)
    : mapping(mapping), id(next_id++) {
  for (auto it = mapping.begin(); it != mapping.end(); it++) {
    inv_mapping[it->second] = it->first;
  }
}

Permutation::~Permutation() {
  all_perms[get_id()] = NULL;
}

Lit Permutation::operator()(const Lit &l, bool forward) {
  map<Lit, Lit> &m = forward ? mapping : inv_mapping;
  if (m.find(l) != m.end()) {
    return m[l];
  } else {
    return l;
  }
}

set<Lit> Permutation::operator()(const set<Lit> &lits, bool forward) {
  set<Lit> news;
  for (auto lit = lits.cbegin(); lit != lits.cend(); lit++) {
    news.insert((*this)(*lit, forward));
  }

  return news;
}

bool Permutation::operator==(const Permutation &rhs) {
  return id == rhs.id || mapping == rhs.mapping;
}

set<Var> Permutation::support() const {
  set<Var> toret;

  for (auto it = mapping.cbegin(); it != mapping.cend(); it++) {
    toret.insert(it->first.var());
  }

  return toret;
}

Var Permutation::next_in_support(Var v) const {
  set<Var> supp = support();

  Var next = UINT32_MAX;
  for (auto nv = supp.begin(); nv != supp.end(); nv++) {
    if (*nv > v && *nv < next) {
      next = *nv;
    }
  }

  return next;
}

Var Permutation::min_var_in_support() const {
  set<Var> supp = support();
  auto current = supp.begin();
  Var v = *current;

  while (current != supp.end()) {
    if (*current < v) {
      v = *current;
    }
    current++;
  }

  return v;
}
