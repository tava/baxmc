/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include <approxmc/approxmc.h>
#include <baxmc.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <cassert>
#include <complex>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <ios>
#include <iostream>
#include <iterator>
#include <ostream>
#include <problem.hpp>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

using namespace ApproxMC;
using namespace CMSat;
namespace po = boost::program_options;
using std::string;

#define INPUT_FILE "input-file"

int main(int argc, char **argv) {
  po::options_description precision("Precision options");
  double epsilon;
  double delta;
  double kappa;
  bool progressive;
  bool dicho;
  size_t max_iter;
  size_t verbosity;
  size_t seed;
  string polarity;
  string dheur_str;
  string perffile;
  precision.add_options()
    ("epsilon,e", po::value<double>(&epsilon)->default_value(0.8), "Epsilon value of PAC guarantees")
    ("delta,d", po::value<double>(&delta)->default_value(0.2), "Delta value of PAC guarantees")
    ("kappa,k", po::value<double>(&kappa)->default_value(0.8), "Kappa value of PAC guarantees")
    ("max_iter,m", po::value<size_t>(&max_iter)->default_value(0), "Maximum number of iterations to do, 0 to not bound")
    ("dichotomic,D", po::value<bool>(&dicho)->default_value(false), "Enable dichotomic search");

  po::options_description preproc("Solve options");
  preproc.add_options()
    ("symetry-breaking,S", "Break symetries")
    ("polarity,P", po::value<string>(&polarity)->default_value("rnd"),
      "Polarity to use in the solver (rnd, pos, neg, cache).")
    ("forward-progressive,fp", po::value<bool>(&progressive)->default_value(true), "Progressively build models when going forward")
    ("decision-heuristic,H", po::value<string>(&dheur_str)->default_value("leads"), "Decision heuristic used when picking variables (leads, vsids, solver, rnd)");

  po::options_description desc("Main options");
  desc.add_options()
    ("help,h", "print this help message")
    ("stats,s", "only print problem statistics")
    ("verbosity,v", po::value<size_t>(&verbosity)->default_value(1), "be more verbose")
    (INPUT_FILE, po::value<std::vector<string>>(), "input file")
    ("perf,p", po::value<string>(&perffile), "performance logging file output")
    ("random-seed,r", po::value<size_t>(&seed)->default_value(12345), "Random seed");

  desc.add(preproc);
  desc.add(precision);

  srand(seed);
  srandom(seed);

  po::positional_options_description positionals;
  positionals.add(INPUT_FILE, -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv)
                .options(desc)
                .positional(positionals)
                .run(),
            vm);
  po::notify(vm);

  // Now use the options
  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return 0;
  }

  BaxMC::PolarityOption pol;
  if (polarity == "rnd") {
    pol = BaxMC::PORandom;
  } else if (polarity == "pos") {
    pol = BaxMC::POPositive;
  } else if (polarity == "neg") {
    pol = BaxMC::PONegative;
  } else if (polarity == "cache") {
    pol = BaxMC::POCached;
  } else {
    printf("Invalid polarity value: %s, expected one of: rnd, pos, neg, cache", polarity.c_str());
    return 0;
  }

  BaxMC::DecisionHeuristic heur;

  if (dheur_str == "leads") {
    heur = BaxMC::DHLeads;
  } else if (dheur_str == "vsids") {
    heur = BaxMC::DHVSIDS;
  } else if (dheur_str == "solver") {
    heur = BaxMC::DHSolver;
  } else if (dheur_str == "rnd") {
    heur = BaxMC::DHRandom;
  } else {
    printf("Invalid decision heuristic value: %s, expected one of: leads, vsids, solver", dheur_str.c_str());
    return 0;
  }

  std::vector<string> files;

  if (vm[INPUT_FILE].empty()) {
    std::cout << "c No files specified, will read from stdin..." << std::endl;
    files.push_back("/dev/stdin");
  } else {
    files = vm[INPUT_FILE].as<std::vector<string>>();
  }

  for (auto fname = files.begin(); fname != files.end(); fname++) {
    BaxMC::Problem p(*fname);
    printf("c %s with %lu counting vars, %lu witness vars, %lu intermediates and %lu clauses.\n",
           fname->c_str(), p.proj.size(), p.target.size(), p.nVars - (p.proj.size() + p.target.size()), p.clauses.size());

    if (vm.count("symetry-breaking")) {
      p.analyze_symetries(verbosity);
    }

    if (!vm.count("stats")) {
      BaxMC::BMC solver(p, max_iter, kappa, pol, heur, progressive);
      if (!perffile.empty()) {
        solver.set_perf_logging(perffile.c_str());
      }
      solver.set_verbosity(verbosity);
      solver.set_delta(delta);
      solver.set_epsilon(epsilon);
      solver.set_dichotomic(dicho);

      (void)solver.solve();
    }
  }

  return 0;
}
