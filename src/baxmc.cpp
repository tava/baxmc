/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include "approxmc/approxmc.h"
#include "cryptominisat5/solvertypesmini.h"
#include "permutation.hpp"
#include "types.hpp"
#include <algorithm>
#include <baxmc.hpp>
#include <bits/types/time_t.h>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <locale>
#include <ostream>
#include <problem.hpp>
#include <sys/resource.h>

#define SOLCOUNT_ZERO                                                          \
  (SolCount{.valid = true, .hashCount = 0, .cellSolCount = 0})

using ApproxMC::AppMC;
using ApproxMC::SolCount;
using std::cout;
using std::endl;
using std::ofstream;
using std::unordered_set;
using std::vector;
using namespace CMSat;

using namespace BaxMC;

const double VSIDS_DECAY = 0.5;

#define CONTAINS(set, lit) ((set).find(lit) != (set).end())

#define SOLCOUNT_OPERATOR(op)                                                  \
  inline bool operator op(const SolCount &lhs, const SolCount &rhs) {          \
    assert(lhs.valid &&rhs.valid);                                             \
    if (lhs.hashCount >= rhs.hashCount) {                                      \
      return lhs.cellSolCount op(rhs.cellSolCount >>                           \
                                 (lhs.hashCount - rhs.hashCount));             \
    } else {                                                                   \
      return (lhs.cellSolCount >> (rhs.hashCount - lhs.hashCount))             \
          op rhs.cellSolCount;                                                 \
    }                                                                          \
  }

SOLCOUNT_OPERATOR(==);
SOLCOUNT_OPERATOR(!=);
SOLCOUNT_OPERATOR(>=);
SOLCOUNT_OPERATOR(>);
SOLCOUNT_OPERATOR(<);

inline SolCount operator+(const SolCount &lhs, const SolCount &rhs) {
  assert(lhs.valid && rhs.valid);
  SolCount n;
  if (lhs.hashCount >= rhs.hashCount) {
    n.hashCount = rhs.hashCount;
    n.cellSolCount = rhs.cellSolCount +
                     (lhs.cellSolCount << (lhs.hashCount - rhs.hashCount));
  } else {
    n.hashCount = lhs.hashCount;
    n.cellSolCount = lhs.cellSolCount +
                     (rhs.cellSolCount << (rhs.hashCount - lhs.hashCount));
  }
  n.valid = true;
  return n;
}

inline SolCount operator>>(const SolCount &lhs, size_t amount) {
  assert(lhs.valid);
  SolCount n;
  if (lhs.hashCount >= amount) {
    n.hashCount = lhs.hashCount - amount;
    n.cellSolCount = lhs.cellSolCount;
  } else {
    n.hashCount = 0;
    n.cellSolCount = lhs.cellSolCount >> (amount - lhs.cellSolCount);
  }

  n.valid = true;
  return n;
}

inline SolCount operator*(const SolCount &lhs, const double &rhs) {
  assert(lhs.valid);

  SolCount dest(lhs);

  if (rhs < 1.) {
    // Avoid loosing too much precision by making the count bigger
    while (dest.hashCount > 0 && dest.cellSolCount < (UINT32_MAX >> 1)) {
      dest.hashCount--;
      dest.cellSolCount <<= 1;
    }
  }

  dest.cellSolCount = (uint32_t)std::floor(dest.cellSolCount * rhs);
  dest.valid = true;
  return dest;
}

inline SolCount operator/(const SolCount &lhs, const double &rhs) {
  return lhs * (1 / rhs);
}

inline bool is_included(const unordered_set<Lit> &small,
                        const unordered_set<Lit> &big) {
  if (small.size() > big.size()) {
    return false;
  }

  for (auto &lit : small) {
    if (!CONTAINS(big, lit)) {
      return false;
    }
  }

  return true;
}

BMC::BMC(Problem p, size_t max_iterations, double kappa, PolarityOption pol,
         DecisionHeuristic heur, bool forward_progressive)
    : mc_calls(0), problem(p), epsilon(0.9), delta(0.2), kappa(kappa),
      current_lead(Lead{
          .count = SOLCOUNT_ZERO,
          .seed = unordered_set<Lit>({}),
      }),
      forward_progressive(forward_progressive), polarity(pol), heuristic(heur),
      curmax(SOLCOUNT_ZERO), dicho_enabled(false), verbosity(0),
      max_iterations(max_iterations), outfile(ofstream()) {
  solver.new_vars(p.nVars);

  switch (pol) {
  case PORandom:
    solver.set_polarity_random();
    break;
  case POPositive:
    solver.set_default_polarity(true);
    break;
  case PONegative:
    solver.set_default_polarity(false);
    break;
  case POCached:
    solver.set_polarity_auto();
    break;
  }

  for (auto &var : problem.target) {
    vsids[Lit(var, false)] = 0.0;
    vsids[Lit(var, true)] = 0.0;
  }

  vector<uint32_t> vars;
  vars.insert(vars.begin(), p.proj.begin(), p.proj.end());

  vector<Lit> clause_tmp;
  for (auto &clause : problem.clauses) {
    clause_tmp.insert(clause_tmp.begin(), clause.begin(), clause.end());
    solver.add_clause(clause_tmp);
    clause_tmp.clear();
  }

  for (auto &pid : problem.perm_ids) {
    perm_activities[pid] = true;
    Permutation *p = Permutation::get_perm(pid);

    perm_table[pid] = p->min_var_in_support();
  }
}

unordered_set<Lit> model_to_track(vector<lbool> model,
                                  unordered_set<Var> projection) {
  unordered_set<Lit> to_return;
  for (size_t index = 0; index < model.size(); index++) {
    if (projection.find(index) != projection.end() && model[index] != l_Undef) {
      to_return.insert(Lit(index, model[index] == l_False));
    }
  }
  return to_return;
}

void BMC::set_epsilon(double e) { epsilon = e; }

void BMC::set_delta(double d) { delta = d; }

void BMC::set_verbosity(size_t verb) { verbosity = verb; }

void BMC::set_dichotomic(bool dicho) { dicho_enabled = dicho; }

vector<Lit> track_to_assumps(unordered_set<Lit> track) {
  return vector<Lit>(track.begin(), track.end());
}

void BMC::reset_counter() {}

ApproxMC::SolCount BMC::count_candidate(unordered_set<CMSat::Lit> candidate,
                                        double eps, double del, bool learn) {
  AppMC counter;
  assert(eps > 0.0 && del > 0.0);
  counter.new_vars(problem.nVars);
  counter.set_epsilon(eps);
  counter.set_delta(del);

  if (verbosity >= 4) {
    printf("c Counting with bounds epsilon=%f delta=%f\n", eps, del);
  }

  // Set up variables and projections variables
  counter.set_projection_set(vector(problem.proj.begin(), problem.proj.end()));

  vector<Lit> clause_tmp;
  for (auto &clause : problem.clauses) {
    clause_tmp.insert(clause_tmp.begin(), clause.begin(), clause.end());
    counter.add_clause(clause_tmp);
    clause_tmp.clear();
  }

  // Add the literals
  for (auto &lit : candidate) {
    clause_tmp.push_back(lit);
    counter.add_clause(clause_tmp);
    clause_tmp.clear();
  }

  if (candidate.size() < problem.target.size()) {
    // Not a complete witness, we must use the pivot variable
    clause_tmp.push_back(Lit(pivot_var, true));
    counter.add_clause(clause_tmp);
    clause_tmp.clear();
  }

  mc_calls++;
  SolCount c = counter.count();
  assert(c.valid);

  if (learn && c > curmax) {
    Lead l;
    l.seed = candidate;
    l.count = c;
    insert_lead(l);
  }
  return c;
}

SolCount BMC::count_candidate(double eps, double del) {
  return count_candidate(unordered_set<Lit>(), eps, del, false);
}

void BMC::insert_lead(Lead lead) {
  if (lead.seed.empty()) {
    return;
  }

  // If there is leads with lower counts that are extension of this lead, add
  // the negation of a decision in this
  // This pass makes sure that all leads' subspaces are disjoint from the leads
  // below its count
  for (auto other = leads.rbegin(); other != leads.rend(); other++) {
    if (other->count > lead.count) {
      continue;
    }
    if (is_included(lead.seed, other->seed)) {
      for (auto lit = other->seed.begin(); lit != other->seed.end(); lit++) {
        if (!CONTAINS(lead.seed, *lit) && !CONTAINS(lead.seed, ~(*lit))) {
          unordered_set<Lit> tmp_seed(lead.seed);
          tmp_seed.insert(~(*lit));
          if (is_candidate_possible(tmp_seed)) {
            lead.seed = tmp_seed;
            // continue to the other lead to see if I can add another decision
            break;
          } else {
            lead.seed.insert(*lit);
          }
        }
      }
    }
  }

  // Just make sure it is filled with every implied literals.
  vector<Lit> assumps = track_to_assumps(lead.seed);
  vector<Lit> implied;
  if (solver.implied_by(assumps, implied)) {
    for (auto &lit : implied) {
      if (CONTAINS(problem.target, lit.var())) {
        lead.seed.insert(lit);
      }
    }
  }

  auto dest = leads.begin();

  while (dest != leads.end() &&
         (lead.count > dest->count || (lead.count == dest->count &&
                                       lead.seed.size() > dest->seed.size()))) {
    if (is_included(dest->seed, lead.seed)) {
      // This seed specializes the lead, to something that has _at least_ the
      // same count
      leads.erase(dest);
    } else {
      // is_included(lead.seed, dest->seed) should not happen
      // unless lead.seed == dest->seed because here lead.count >= dest->count
      dest++;
    }
  }

  dest = leads.insert(dest, lead);
  dest++;

  // Now continue and possibly remove other leads
  while (dest != leads.end()) {
    if (is_included(lead.seed, dest->seed)) {
      // This lead is a generalization of a previous lead, with a lower count,
      // we can thus remove the previous lead
      leads.erase(dest);
    } else {
      // is_included(dest->seed, lead.seed) is impossible because dest->count >=
      // lead.count
      dest++;
    }
  }

  for (auto &lit : lead.seed) {
    lead_counts[lit] += lead.count.cellSolCount;
  }
}

Lead BMC::pop_lead() {
  Lead tmp = leads.back();
  leads.pop_back();
  return tmp;
}

void BMC::add_clause(unordered_set<Lit> clause) {
  vector<Lit> vec_clause;
  vec_clause.insert(vec_clause.begin(), clause.begin(), clause.end());

  problem.add_clause(clause);
  solver.add_clause(vec_clause);
}

void BMC::block(unordered_set<Lit> conjunction) {
  unordered_set<Lit> set_blocker;

  for (auto &it : vsids) {
    vsids[it.first] *= VSIDS_DECAY;
  }

  for (auto &lit : conjunction) {
    set_blocker.insert(~lit);
    vsids[lit] += 1;
  }
  add_clause(set_blocker);
}

void fill_with_removed(unordered_set<Lit> original, unordered_set<Lit> removed,
                       unordered_set<Lit> &dest) {
  dest.clear();
  for (auto &to_insert : original) {
    if (!CONTAINS(removed, to_insert)) {
      dest.insert(to_insert);
    }
  }
}

uint32_t compute_k(const SolCount best, const SolCount current) {
  uint32_t log_part = (uint32_t)std::floor(std::log2(best.cellSolCount) -
                                           std::log2(current.cellSolCount));
  if (best.hashCount >= current.hashCount) {
    return (best.hashCount - current.hashCount) + log_part;
  } else {
    return 0;
  }
}

#define IF_LIT_IMPOSSIBLE(solver, assumps, lit, todo)                          \
  {                                                                            \
    (assumps).push_back((lit));                                                \
    if ((solver).solve(&(assumps)) == l_False) {                               \
      (assumps).pop_back();                                                    \
      { todo }                                                                 \
    } else {                                                                   \
      (assumps).pop_back();                                                    \
    }                                                                          \
  }
unordered_set<Lit> BMC::next_lits_forward(unordered_set<Lit> track,
                                          unordered_set<Lit> &redundant) {
  vector<Lit> assumps = track_to_assumps(track);
  assumps.push_back(Lit(pivot_var, true));
  unordered_set<Lit> news;

  unordered_set<Lit> all_lits;
  for (auto &it : leads) {
    all_lits.insert(it.seed.begin(), it.seed.end());
  }

  Lit next = lit_Undef;

  if (heuristic == DHLeads) {
    double maxvs = 0.0;
    (void)solver.solve(&assumps);
    for (auto &lit : all_lits) {
      if (!CONTAINS(track, lit) && !CONTAINS(track, ~lit) &&
          !CONTAINS(news, ~lit)) {

        IF_LIT_IMPOSSIBLE(solver, assumps, lit, {
          news.insert(~lit);
          redundant.insert(~lit);
          learn_conflict();
          continue;
        })

        if (vsids[lit] > maxvs) {
          next = lit;
          maxvs = vsids[lit];
        }
      }
    }
  } else if (heuristic == DHVSIDS) {
    double max = 0.0;
    for (auto &var : problem.target) {
      Lit poslit = Lit(var, false);
      Lit neglit = Lit(var, true);
      if (CONTAINS(track, poslit) || CONTAINS(track, neglit)) {
        continue;
      }

      IF_LIT_IMPOSSIBLE(solver, assumps, poslit, {
        news.insert(neglit);
        redundant.insert(neglit);
        learn_conflict();
        continue;
      })
      IF_LIT_IMPOSSIBLE(solver, assumps, neglit, {
        news.insert(poslit);
        redundant.insert(poslit);
        learn_conflict();
        continue;
      })

      // At this point, we need to make a polarity decision
      double pos_vs = vsids[poslit];
      double neg_vs = vsids[neglit];

      if (pos_vs > max) {
        next = poslit;
        max = pos_vs;
      }

      if (neg_vs > max) {
        next = neglit;
        max = neg_vs;
      }
    }
  } else if (heuristic == DHRandom) {
    vector<Lit> possible_lits;
    for (auto &var : problem.target) {
      Lit poslit = Lit(var, false);
      Lit neglit = Lit(var, true);

      if (CONTAINS(track, poslit) || CONTAINS(track, neglit)) {
        continue;
      }

      IF_LIT_IMPOSSIBLE(solver, assumps, poslit, {
        news.insert(neglit);
        redundant.insert(neglit);
        learn_conflict();
        continue;
      })
      IF_LIT_IMPOSSIBLE(solver, assumps, neglit, {
        news.insert(poslit);
        redundant.insert(poslit);
        learn_conflict();
        continue;
      })

      possible_lits.push_back(poslit);
      possible_lits.push_back(neglit);
    }
    if (!possible_lits.empty()) {
      std::random_shuffle(possible_lits.begin(), possible_lits.end());
      next = possible_lits.back();
    }
  }

  if (next == lit_Undef) {
    // Can't decide anything, so just get a solution out of the solver
    if (verbosity >= 4) {
      printf("c Current track is fresh\n");
    }
    assert(solver.solve(&assumps) == l_True);
    return model_to_track(solver.get_model(), problem.target);
  }

  vector<Lit> implied;
  assumps.push_back(next);
  news.insert(next);
  solver.implied_by(assumps, implied);
  vector<Lit> zero_implied = solver.get_zero_assigned_lits();
  implied.insert(implied.begin(), zero_implied.begin(), zero_implied.end());
  for (auto &lit : implied) {
    if (CONTAINS(problem.target, lit.var()) && !(CONTAINS(track, lit)) &&
        lit != next) {
      news.insert(lit);
      redundant.insert(lit);
    }
  }
  return news;
}

unordered_set<Lit> BMC::next_lits_backward(unordered_set<Lit> track,
                                           unordered_set<Lit> removed,
                                           unordered_set<Lit> ignored,
                                           size_t n) {
  unordered_set<Lit> accumulator;
  vector<Lit> sorted(track.begin(), track.end());

  if (n > track.size() - removed.size()) {
    return accumulator;
  }

  if (heuristic == DHVSIDS) {
    std::sort(sorted.begin(), sorted.end(),
              [this](Lit a, Lit b) { return vsids[a] > vsids[b]; });
  } else if (heuristic == DHLeads) {
    std::sort(sorted.begin(), sorted.end(),
              [this](Lit a, Lit b) { return lead_counts[a] < lead_counts[b]; });
  } else if (heuristic == DHRandom) {
    std::random_shuffle(sorted.begin(), sorted.end());
  }

  for (auto lit = sorted.begin(); lit != sorted.end() && accumulator.size() < n;
       lit++) {
    // Check if this one is removed
    if (!CONTAINS(removed, *lit) && !CONTAINS(ignored, *lit)) {
      accumulator.insert(*lit);
    }
  }

  return accumulator;
}

void BMC::learn_conflict() {
  // Before that, add th learnt conflict to the problem.
  vector<Lit> conflict = solver.get_conflict();
  add_clause(unordered_set(conflict.begin(), conflict.end()));
}

bool BMC::is_candidate_possible(unordered_set<CMSat::Lit> candidate,
                                bool learn) {
  vector<Lit> assumps(candidate.begin(), candidate.end());
  assumps.push_back(Lit(pivot_var, true));

  if (solver.solve(&assumps) == l_True) {
    return true;
  }

  if (learn) {
    learn_conflict();
  }
  return false;
}

void BMC::log_bounds() {
  if (verbosity >= 1) {
    cout << "c Bounds " << curmax << " " << supermax << endl;
  }
  report_statistics();
}

Var BMC::get_new_pivot_var() {
  assert(solver.nVars() == problem.nVars);
  Var v = problem.nVars;

  problem.nVars++;
  solver.new_var();
  assert(solver.nVars() == problem.nVars);

  return v;
}

void BMC::stage_assumptions(bool validate) {
  if (verbosity && dicho_enabled) {
    if (validate) {
      cout << "c Validating new assumptions" << endl;
    } else {
      cout << "c Reverting wrong assumptions" << endl;
    }
  }
  add_clause(unordered_set({Lit(pivot_var, validate)}));
  pivot_var = get_new_pivot_var();
  did_add_assumptions = false;
}

SolCount BMC::compute_supermax(double epsilon, double delta) {
  SolCount tmp = count_candidate(epsilon, delta);
  if (enhanced_supermax.valid && enhanced_supermax < tmp) {
    return enhanced_supermax;
  } else {
    return tmp;
  }
}

std::pair<SolCount, unordered_set<Lit>> BMC::solve() {
  time_t start_time = std::time(NULL);
  size_t iterations_count = 0;
  double epsilon0 = std::cbrt(1 + epsilon) - 1;
  double epsilon1 = epsilon0;
  if (kappa > epsilon0) {
    kappa = epsilon0;
  }
  double delta0 = delta / 2;
  double delta1 = delta / (2 * (problem.target.size() + 1));
  double delta2 = delta / 2;

  double epsilonG = epsilon;
  double deltaG = delta2 / (problem.target.size() + 1);

  SolCount dicho_max = SolCount{.valid = false};
  bool did_try_assumptions = false;
  did_add_assumptions = false;
  pivot_var = get_new_pivot_var();

  supermax = compute_supermax(epsilon0, delta0);
  log_bounds();

  while (did_add_assumptions ||
         (supermax.cellSolCount != 0 && curbest.empty()) ||
         (curmax < supermax / (1 + kappa) &&
          (max_iterations == 0 || max_iterations > iterations_count))) {
    iterations_count++;

    if (did_add_assumptions && curmax >= supermax / (1 + kappa)) {
      // We were too aggressive, we have to retract our assumptions and restart
      stage_assumptions(false);
      enhanced_supermax = dicho_max;
      supermax = compute_supermax(epsilon0, delta0);
      dicho_max.valid = false;

      // Just to be sure
      assert(enhanced_supermax.valid);
      // Loop once more
      continue;
    }

  restart:
    if (!leads.empty()) {
      // Clean the leads queue to remove unsatisfiable leads
      while (!leads.empty() && !is_candidate_possible(leads.back().seed)) {
        leads.pop_back();
      }

      current_lead.seed.clear();
      if (!leads.empty()) {
        current_lead = pop_lead();
        track = current_lead.seed;
        report_statistics();
        if (verbosity > 3) {
          cout << "c Picked a lead with " << current_lead.count << endl;
        }
      }
    }

    update_cancel();
    assert(is_candidate_possible(track));
    // Compute dichotomic value of curmax if possible and needed
    if (!dicho_max.valid) {
      if (dicho_enabled && curmax.valid && curmax.cellSolCount > 0 &&
          !(!did_add_assumptions && did_try_assumptions)) {
        dicho_max = (curmax + supermax) / 2.;
      } else {
        dicho_max = curmax;
      }
    }

    if (verbosity && dicho_enabled) {
      cout << "c Trying against " << dicho_max << " over " << curmax << endl;
    }

    /////////////////////////////////////////////////////////////////////////
    // Forward
    unordered_set<Lit> removed;

    SolCount c;
    c.cellSolCount = 0;

    unordered_set<Lit> extra_lits;
    if (forward_progressive) {
      do {
        extra_lits = next_lits_forward(track, removed);
        track.insert(extra_lits.begin(), extra_lits.end());

        if (verbosity >= 4) {
          printf("c Added %lu decicions\n", extra_lits.size());
        }

        // Check symetry here
        update_assign();
        if (check_and_block_lexleader()) {
          track.clear();
          goto restart;
        }

        c = count_candidate(track, epsilon1, delta1);
      } while (c > dicho_max && track.size() < problem.target.size());
    } else {
      do {
        extra_lits = next_lits_forward(track, removed);
        track.insert(extra_lits.begin(), extra_lits.end());

        if (verbosity >= 4) {
          printf("c Added %lu decicions\n", extra_lits.size());
        }

        // Check symetry here
        update_assign();
        if (check_and_block_lexleader()) {
          track.clear();
          goto restart;
        }

      } while (track.size() < problem.target.size());
      c = count_candidate(track, epsilon1, delta1);
    }

    if (c > dicho_max) {
      // Neat, we enhanced the known maximum.
      // Now, block this, then crawl the leads list in ascending order, and
      // block everyone that is below our current solution.
      assert(track.size() == problem.target.size());
      block(track);
      curmax = c;
      curbest = track;
      track.clear();
      if (curmax > current_lead.count) {
        current_lead.seed.clear();
      } else {
        insert_lead(current_lead);
      }
      log_bounds();

      if (!leads.empty()) {
        auto lead_it = leads.begin();

        while (lead_it != leads.end() && curmax > lead_it->count) {
          block(lead_it->seed);
          lead_it++;
        }

        if (verbosity >= 3) {
          cout << "c Erasing " << lead_it - leads.begin() << " leads over "
               << leads.size() << endl;
        }
        leads.erase(leads.begin(), lead_it);
      }

      stage_assumptions(true);
      did_try_assumptions = false;
      dicho_max.valid = false;
      continue;
    }

    if (verbosity >= 3) {
      printf("c Switching direction with %lu redundant literals\n",
             removed.size());
    }
    /////////////////////////////////////////////////////////////////////////
    // Backward
    unordered_set<Lit> tmp_track;
    unordered_set<Lit> ignored;

    // Log elimination
    uint32_t k = compute_k(dicho_max, c);
    while (k > 0) {
      tmp_track.clear();
      unordered_set<Lit> tmp_removed =
          next_lits_backward(track, removed, ignored, k);

      if (tmp_removed.empty()) {
        // We asked for too much in one go..
        break;
      }

      for (auto &lit : track) {
        if (!CONTAINS(removed, lit) && !CONTAINS(tmp_removed, lit)) {
          tmp_track.insert(lit);
        }
      }

      c = count_candidate(tmp_track, epsilonG, deltaG);
      if (dicho_max / (1 + epsilon) >= c) {
        removed.insert(tmp_removed.begin(), tmp_removed.end());
        k = compute_k(dicho_max, c);
        tmp_removed.clear();
      } else {
        break;
      }
    }

    // Rafinement
    for (unordered_set<Lit> to_add =
             next_lits_backward(track, removed, ignored, 1);
         to_add.size() > 0;
         to_add = next_lits_backward(track, removed, ignored, 1)) {
      auto lit = to_add.begin();

      fill_with_removed(track, removed, tmp_track);
      tmp_track.erase(*lit);

      // Check redundancy
      tmp_track.insert(~(*lit));
      if (!is_candidate_possible(tmp_track)) {
        removed.insert(*lit);
        tmp_track.clear();
        continue;
      } else {
        // Not redundant... Restore
        tmp_track.erase(~(*lit));
      }

      assert(tmp_track.size() + removed.size() == track.size() - 1);

      c = count_candidate(tmp_track, epsilonG, deltaG);
      if (dicho_max / (1 + epsilon) >= c) {
        removed.insert(*lit);
      } else {
        ignored.insert(*lit);
      }
      tmp_track.clear();
    }
    fill_with_removed(track, removed, tmp_track);
    if (dicho_max != curmax) {
      did_add_assumptions = true;
      did_try_assumptions = true;
      tmp_track.insert(Lit(pivot_var, true));
    }
    block(tmp_track);
    reset_counter();
    track.clear();

    SolCount prevmax = supermax;
    supermax = compute_supermax(epsilon0, delta0);
    if (current_lead.seed.size() > 0 &&
        is_candidate_possible(current_lead.seed)) {
      current_lead.count =
          count_candidate(current_lead.seed, epsilon1, delta1, false);
      if (current_lead.count > dicho_max) {
        insert_lead(current_lead);
      } else {
        if (verbosity) {
          cout << "c Blocking a lead" << endl;
        }
        if (dicho_max != curmax && current_lead.count > curmax) {
          did_add_assumptions = true;
          did_try_assumptions = true;
          current_lead.seed.insert(Lit(pivot_var, true));
        }
        block(current_lead.seed);
      }
    } else if (prevmax != supermax) {
      log_bounds();
    }
  }

  if (verbosity) {
    printf("c Did a total of %zu MC calls\n", mc_calls);
    printf("c Total running time of %u seconds\n",
           (uint32_t)std::difftime(std::time(NULL), start_time));
    cout << "v " << curmax << endl;
    cout << "s " << curbest << endl;
  }
  return std::pair<SolCount, unordered_set<Lit>>(curmax, curbest);
}

void BMC::update_assign() {
  for (auto &pid : problem.perm_ids) {
    if (!perm_activities[pid]) {
      continue;
    }

    Var v = perm_table[pid];
    Lit l = Lit(v, false);
    Permutation *p = Permutation::get_perm(pid);
    while ((CONTAINS(track, l) && CONTAINS(track, (*p)(l, false))) ||
           (CONTAINS(track, ~l) && CONTAINS(track, ~(*p)(l, false)))) {
      v = p->next_in_support(v);
      l = Lit(v, false);
    }
    perm_table[pid] = v;
  }
}

void BMC::update_cancel() {
  for (auto &pid : problem.perm_ids) {
    Permutation *p = Permutation::get_perm(pid);
    std::set<Var> psup = p->support();
    Var u = UINT32_MAX;

    for (auto &var : psup) {
      Lit vlit = Lit(var, false);
      if (var <= u && (CONTAINS(track, vlit) || CONTAINS(track, ~vlit) ||
                       CONTAINS(track, (*p)(vlit, false)) ||
                       CONTAINS(track, ~(*p)(vlit, false)))) {
        u = var;
      }
    }

    if (u <= perm_table[pid]) {
      if (verbosity >= 4) {
        printf("c Symetry %lu is now active\n", pid);
      }
      perm_table[pid] = u;
      perm_activities[pid] = true;
    }
  }
}

bool BMC::check_and_block_lexleader() {
  for (auto &pid : problem.perm_ids) {
    if (!perm_activities[pid]) {
      continue;
    }

    Permutation *p = Permutation::get_perm(pid);
    Var v = perm_table[pid];
    Lit vlit = Lit(v, false);

    if (CONTAINS(track, vlit) && CONTAINS(track, (*p)(~vlit, false))) {
      // Compute the esbp
      unordered_set<Lit> learnt_set;
      Var minv = p->min_var_in_support();
      Var u = perm_table[pid];
      Lit minvlit = Lit(minv, false);
      while (u != minv) {
        if (CONTAINS(track, minvlit)) {
          learnt_set.insert(~minvlit);
        } else {
          learnt_set.insert(minvlit);
        }

        if (CONTAINS(track, (*p)(minvlit, false))) {
          learnt_set.insert(~(*p)(minvlit, false));
        } else {
          learnt_set.insert((*p)(minvlit, false));
        }
        minv = p->next_in_support(minv);
        minvlit = Lit(minv, false);
      }
      learnt_set.insert(~minvlit);
      learnt_set.insert((*p)(minvlit, false));

      if (verbosity >= 1) {
        printf("c Breaking symetry %lu\n", pid);
      }
      add_clause(learnt_set);
      return true;
    }

    if (CONTAINS(track, ~vlit) && CONTAINS(track, (*p)(vlit, false))) {
      if (verbosity >= 4) {
        printf("c Symetry %lu is now inactive\n", pid);
      }
      perm_activities[pid] = false;
    }
  }

  return false;
}

void BMC::set_perf_logging(const char *const out) {
  outfile.open(out);
  if (outfile.is_open()) {
    outfile << "time,max,supermax,lead" << endl;
  }
}

void BMC::report_statistics() {
  if (outfile.is_open()) {
    struct rusage ru;
    getrusage(RUSAGE_SELF, &ru);
    uint64_t msec_time = (ru.ru_utime.tv_sec + ru.ru_stime.tv_sec) * 1E6 +
                         ru.ru_utime.tv_usec + ru.ru_stime.tv_usec;

    if (current_lead.seed.empty()) {
      outfile << msec_time << "," << curmax << "," << supermax << ",0" << endl;
    } else {
      outfile << msec_time << "," << curmax << "," << supermax << ","
              << current_lead.count << endl;
    }
  }
}

Lit BMC::polarity_decide(Var v) {
  switch (polarity) {
  case PORandom:
    return Lit(v, (random() % 2 == 0));
  case POPositive:
    return Lit(v, false);
  // TODO: actually make a decision in the cached case
  case POCached:
  case PONegative:
    return Lit(v, true);
  }

  return lit_Undef;
}
