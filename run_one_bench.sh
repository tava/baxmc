#!/bin/sh
. ./utils.sh

echo "Running $1"
rm -f $1.log $1.sym.log $1.maxcount.log
# run_cmd "$1.log" ./build/baxmc --epsilon=0.8 --delta=0.2 --kappa=0.8 -v 1 "$1"
run_cmd "$1.sym.log" ./build/baxmc -S --epsilon=0.8 --delta=0.2 --kappa=0.8 -v 1 "$1"
# run_cmd "$1.maxcount.log" ./maxcount.py 0.8 0.2 $1
