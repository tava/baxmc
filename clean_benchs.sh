#!/bin/sh
echo "Cleaning previous runs..."
find . -name "*.log" -or -name "*.csv" | xargs rm -f
