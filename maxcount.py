#!/usr/bin/env python3
# BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
# 
# By
# 
# Thomas Vigouroux
# Cristian Ene
# David Monniaux
# Laurent Mounier
# Marie-Laure Potet
# 
# This software is a computer program whose purpose is to, given a boolean formula as well as the
# specific role each of the variables appearing in the formula should be considered, find the
# assignement that maximizes the number of models of the formula over some set of variables.
# This specific file is a reimplementaion of the MaxCount algorithm, solving the same problem as
# BaxMC, for the sake of comparison.
# 
# This software is governed by the CeCILL-B license under French law and abiding by the rules of
# distribution of free software. You can use, modify and/ or redistribute the software under the
# terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# 
# As a counterpart to the access to the source code and rights to copy, modify and redistribute
# granted by the license, users are provided only with a limited warranty and the software's
# author, the holder of the economic rights, and the successive licensors have only limited
# liability.
# 
# In this respect, the user's attention is drawn to the risks associated with loading, using,
# modifying and/or developing or reproducing the software by the user in light of its specific
# status of free software, that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced professionals having in-depth
# computer knowledge. Users are therefore encouraged to load and test the software's suitability as
# regards their requirements in conditions enabling the security of their systems and/or data to be
# ensured and, more generally, to use and operate it in the same conditions as regards security.
# 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
# license and that you accept its terms.
# 

from sys import argv
from math import log, log2, ceil, sqrt, inf
from time import time
from subprocess import Popen, PIPE, DEVNULL

PROJ_PREFIX = "c proj "
TARGET_PREFIX = "c target "
IND_PREFIX = "c ind "
PROBLEM_PREFIX = "p "


def clause_to_bytes(clause):
    """
    Turns a clause into a clause line
    """
    return ("{} 0").format(" ".join(map(str, clause)))

def mk_cnf(nbr, clauses, comments=[], ind=[]):
    return comments + [(f"p cnf {nbr} {len(clauses)}")] + list(map(clause_to_bytes, clauses))




def self_product(nbr, clauses, target, proj, amount=1):
    new_proj = proj.copy()
    new_clauses = clauses.copy()

    varmap = dict()
    def get_varmapped(var, index, nbr, target):
        if var in target:
            return var

        if (var, index) in varmap:
            return varmap[(var, index)]

        nvar = nbr + len(varmap) + 1 # Because variables are 1-indexed
        varmap[(var, index)] = nvar
        return nvar

    for j in range(amount):
        # Add the new clauses
        for c in clauses:
            nc = set()
            for lit in c:
                if abs(lit) in target:
                    nc.add(lit)
                else:
                    nvar = get_varmapped(abs(lit), j, nbr, target)
                    nc.add((1 if lit > 0 else -1) * nvar)
                    if abs(lit) in proj:
                        new_proj.add(nvar)
            new_clauses.add(frozenset(nc))

    return nbr + len(varmap), new_proj, new_clauses

def cnf_line_to_clause(line: str):
    return frozenset(map(int, filter(lambda i: len(i) > 0 and i != "0",
                                line.strip().split(" "))))


def read_cnf(fname: str):
    nbr = -1
    proj = set()
    target = set()
    res = set()
    with open(fname, 'r') as file:
        for line in file:
            if line.startswith(TARGET_PREFIX):
                target.update(set(cnf_line_to_clause(line[len(TARGET_PREFIX):])))
            if line.startswith(PROJ_PREFIX):
                proj.update(set(cnf_line_to_clause(line[len(PROJ_PREFIX):])))
            elif line.startswith(PROBLEM_PREFIX):
                nbr = int(line.split(" ")[-2])
            elif not (line.startswith("p") or line.startswith("c")):
                s = cnf_line_to_clause(line)
                if len(s) > 0:
                    res.add(s)
    return (nbr, proj, target, res)

nbr, proj, target, clauses = read_cnf(argv[3])

epsilon = float(argv[1])
delta = float(argv[2])

k = ceil((2 * len(target)) / log2(1 + epsilon))
n = ceil(34 * log2(2 / delta))

start_time = time()

print("c", n, k)
new_nbr, new_proj, new_clauses = self_product(nbr, clauses, target, proj, amount=k)
print("c", nbr, new_nbr)

lines = mk_cnf(new_nbr, new_clauses, comments=[IND_PREFIX + " " +
    clause_to_bytes(target.union(new_proj))])


unigen = Popen(["unigen", f"--samples={ceil(n)}"], stdin=PIPE,
        text=True, stdout=PIPE)
unigen.stdin.write("\n".join(lines))
unigen.stdin.close()

sols = set()
n_candidates = 0
for line in unigen.stdout:
    if not line.startswith("c"):
        n_candidates += 1
        sol = frozenset(b for b in cnf_line_to_clause(line)
                        if abs(b) in target)
        sols.add(sol)

curbest = None
curmax = -inf

for sol in sols:
    # Correct here the epsilon value because the paper uses a rho value that is not really what the
    # paper guarantees
    cmd = ['approxmc', f'--epsilon={sqrt(1 + epsilon) - 1}', f'--delta={delta / (2 * n)}']
    modelcounter = Popen(cmd, stdin=PIPE, stdout=PIPE,
                         stderr=PIPE, text=True)
    lines = mk_cnf(nbr, clauses.union(frozenset({lit}) for lit in sol), comments=[
                   (f"c ind {clause_to_bytes(proj)}"), ])
    lines = "\n".join(lines)
    modelcounter.stdin.write(lines)
    modelcounter.stdin.close()
    out = modelcounter.stdout.readlines()
    res = int(out[-1].split(" ")[-1].strip())

    if res > curmax:
        print("c Enhanced max to", res)
        curbest = sol
        curmax = res

print('v', curmax)
print('s', *curbest)
print(f"c total time of {time() - start_time:.3}")
