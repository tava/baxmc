#!/bin/sh

EXPECTED="expected"
RES="res"

if [ ! -d "build" ]
then
  mkdir -p build && cd build
  cmake ..
  cd ..
fi

make -C build -j 4

./build/btest --gtest_output="xml:report.xml"
