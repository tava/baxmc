/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#ifndef __PROBLEM_HPP_
#define __PROBLEM_HPP_
#include "permutation.hpp"
#include "types.hpp"
#include <approxmc/approxmc.h>
#include <bliss/graph.hh>
#include <cstddef>
#include <fstream>
#include <map>
#include <unordered_set>

template <> struct std::hash<CMSat::Lit> {
  size_t operator()(const CMSat::Lit &lit) const {
    return std::hash<uint32_t>{}(lit.toInt());
  }
};

inline std::ostream &operator<<(std::ostream &os,
                                const std::unordered_set<CMSat::Lit> track) {
  if (!track.empty()) {
    auto lit = track.begin();
    os << *lit;
    lit++;
    for (; lit != track.end(); lit++) {
      os.put(' ');
      os << *lit;
    }
  }

  return os;
}

namespace BaxMC {

/// Immutable unordered set (hashable)
class Clause {
public:
  template <typename InputIt>
  Clause(InputIt start, InputIt stop)
      : Clause(std::unordered_set<CMSat::Lit>(start, stop)){}

  Clause(std::unordered_set<CMSat::Lit> inner);

  bool contains(const Var &v) const;
  bool contains(const CMSat::Lit &lit) const;
  bool operator==(const Clause &other) const;

  std::unordered_set<CMSat::Lit>::const_iterator begin() const {
    return this->inner.cbegin();
  }

  std::unordered_set<CMSat::Lit>::const_iterator end() const {
    return this->inner.cend();
  }

private:
  std::unordered_set<CMSat::Lit> inner;
};

} // namespace BaxMC

template <> struct std::hash<BaxMC::Clause> {
  size_t operator()(const BaxMC::Clause &c) const {
    size_t init = 0xdeadbeef;
    for (CMSat::Lit l : c) {
      init ^= std::hash<CMSat::Lit>{}(l);
    }

    return init;
  }
};

namespace BaxMC {

class Problem {
public:
  std::unordered_set<Clause> clauses;
  size_t nVars;
  std::unordered_set<Var> proj;
  std::unordered_set<Var> target;
  std::vector<size_t> perm_ids;

  Problem(std::string fname);
  Problem(std::ifstream file);

  void add_clause(std::unordered_set<CMSat::Lit> clause) {
    add_clause(Clause(clause));
  };
  void add_clause(Clause clause);

  bool is_projection_var(Var var);
  bool is_target_var(Var var);
  void analyze_symetries(size_t verbosity = 0);

  bool dont_care(Var v, Clause assumps);
  void show_restricted_graph(const char *const fname,
                             Clause assumps);
};

} // namespace BaxMC
#endif
