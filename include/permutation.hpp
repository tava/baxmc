/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#ifndef __PERMUTATION_HPP__
#define __PERMUTATION_HPP__

#include "cryptominisat5/solvertypesmini.h"
#include "types.hpp"
#include <map>
#include <set>
namespace BaxMC {

struct Permutation {
public:
  static Permutation * create(std::map<CMSat::Lit, CMSat::Lit> mapping);
  size_t get_id() const { return id; };
  static size_t nr_perms() { return next_id; };
  std::set<Var> support() const;

  CMSat::Lit operator()(const CMSat::Lit &lit, bool forward = true);
  std::set<CMSat::Lit> operator()(const std::set<CMSat::Lit> &lit, bool forward = true);
  inline bool operator==(const Permutation &rhs);

  static Permutation * get_perm(size_t id) {
    if (id >= next_id) {
      return NULL;
    }

    return all_perms[id];
  }

  Var min_var_in_support() const;
  Var next_in_support(Var v) const;

private:
  Permutation(std::map<CMSat::Lit, CMSat::Lit> mapping);
  ~Permutation();
  std::map<CMSat::Lit, CMSat::Lit> mapping;
  std::map<CMSat::Lit, CMSat::Lit> inv_mapping;
  size_t id;
  static size_t next_id;
  static std::vector<Permutation *> all_perms;
};

} // namespace BaxMC

#endif
