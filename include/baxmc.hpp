/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */
 
#ifndef __BAXMC_HPP__
#define __BAXMC_HPP__
#include "approxmc/approxmc.h"
#include "cryptominisat5/cryptominisat.h"
#include "cryptominisat5/solvertypesmini.h"
#include "types.hpp"
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <ostream>
#include <problem.hpp>
#include <unordered_set>
#include <utility>

inline std::ostream &operator<<(std::ostream &os,
                                const ApproxMC::SolCount count) {
  os << count.cellSolCount * std::pow(2, count.hashCount);
  return os;
}

namespace BaxMC {

enum PolarityOption { PORandom, POPositive, PONegative, POCached };
enum DecisionHeuristic {
  DHLeads,
  DHVSIDS,
  DHRandom,
  DHSolver,
};

/// A possible track for the solver
/// The count value _must_ be greater or equal to the actual count of the seed
struct Lead {
  ApproxMC::SolCount count;
  std::unordered_set<CMSat::Lit> seed;
};

class BMC {
public:
  size_t mc_calls;

  BMC(Problem p, size_t max_iterations = 0, double kappa = 0.9,
      PolarityOption pol = PONegative, DecisionHeuristic heur = DHLeads,
      bool forward_progressive = true);
  std::pair<ApproxMC::SolCount, std::unordered_set<CMSat::Lit>> solve();
  void set_epsilon(double e);
  void set_delta(double d);
  void set_verbosity(size_t verb);

  void set_perf_logging(const char *const outfile);
  void set_dichotomic(bool dicho);

private:
  // Oracles
  BaxMC::Problem problem;
  CMSat::SATSolver solver;
  bool is_candidate_possible(std::unordered_set<CMSat::Lit> candidate, bool learn = true);
  ApproxMC::SolCount count_candidate(std::unordered_set<CMSat::Lit> candidate, double eps, double del,
                                     bool learn = true);
  ApproxMC::SolCount count_candidate(double eps, double del);

  // Precisions
  double epsilon;
  double delta;
  double kappa;

  // Useful to run
  Lead current_lead;
  bool forward_progressive;
  ApproxMC::SolCount supermax;
  PolarityOption polarity;
  DecisionHeuristic heuristic;
  std::vector<Lead> leads;
  std::unordered_set<CMSat::Lit> ignored;
  std::unordered_set<CMSat::Lit> track;
  std::map<CMSat::Lit, double> vsids;
  std::map<CMSat::Lit, size_t> lead_counts;

  // Current best candidate
  ApproxMC::SolCount curmax;
  std::unordered_set<CMSat::Lit> curbest;

  void block(std::unordered_set<CMSat::Lit> conjunction);
  void learn_conflict();
  void reset_counter();
  void add_clause(std::unordered_set<CMSat::Lit> clause);
  void insert_lead(Lead lead);
  Lead pop_lead();

  // Decision making, and polarity deciding
  CMSat::Lit polarity_decide(Var v);

  std::unordered_set<CMSat::Lit> next_lits_forward(std::unordered_set<CMSat::Lit> track,
                                         std::unordered_set<CMSat::Lit> &redudant);
  std::unordered_set<CMSat::Lit> next_lits_backward(std::unordered_set<CMSat::Lit> track,
                                          std::unordered_set<CMSat::Lit> removed,
                                          std::unordered_set<CMSat::Lit> ignored,
                                          size_t n);

  // Symetry handling
  std::map<size_t, bool> perm_activities;
  std::map<size_t, Var> perm_table;

  void update_assign();
  void update_cancel();
  bool check_and_block_lexleader();

  void report_statistics();
  void log_bounds();

  // Dichotomic update
  bool dicho_enabled;
  Var pivot_var;
  bool did_add_assumptions;
  ApproxMC::SolCount enhanced_supermax;
  ApproxMC::SolCount compute_supermax(double epsilon, double delta);
  Var get_new_pivot_var();
  void stage_assumptions(bool validate = false);

  // Verbosity
  size_t verbosity;
  size_t max_iterations;
  std::ofstream outfile;
};

} // namespace BaxMC

#endif
