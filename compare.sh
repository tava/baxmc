#!/bin/sh

echo "Running $1"
. ./utils.sh

for heur in $(echo "leads vsids solver rnd" | tr ' ' '\n')
do
  for polarity in $(echo "rnd pos neg cache" | tr ' ' '\n')
  do
    LOGFILE="$1.$heur.$polarity.log"
    run_cmd $LOGFILE ./build/baxmc -H $heur -P $polarity -v 1 "$1"
  done
done
