#!/bin/sh
if [ ! -d "build" ]
then
  echo "Running CMake before running the benchmarks"
  mkdir build
  nix-shell --pure --command "cd build && cmake .."
fi

if [ -e "build/Makefile" ]
then
  nix-shell --pure --command "make -C build/"
else
  nix-shell --pure --command "ninja -C build/"
fi

if [ ! -e "build/baxmc" ]
then
  echo "Couldn't find ./build/baxmc, try compiling manually"
  return
fi

find benchmarks -name "*.cnf" -or -name "*.dimacs" | xargs -n 1 -P 10 -I "%" -- nix-shell --pure --command "${1:-./run_one_bench.sh} %"
