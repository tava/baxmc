self: super:
{
  cryptominisat = super.cryptominisat.overrideAttrs (
    old: {
      src = super.fetchFromGitHub {
        owner = "msoos";
        repo = "cryptominisat";
        rev = "b8158a09c9ffe606fd31af282b9dc33c244bce9d";
        sha256 = "sha256-IeaiI+uhHN/xvi/K7GtKVVnKhnrL2UmZ2IXi9gI8Gm8=";
      };
      patches = [ ./patches/cmsat.patch ];
    }
  );
}
