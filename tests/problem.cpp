/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include "problem.hpp"
#include "defs.hpp"
#include <fstream>
#include <gtest/gtest.h>
#include <stddef.h>
#include <unordered_set>

using namespace BaxMC;
using namespace CMSat;

TEST(Clause, can_be_constructed) {
  std::unordered_set<Lit> cls({Lit(0, true), Lit(1, false), Lit(2, true)});
  ASSERT_NO_FATAL_FAILURE(Clause c(cls));
}

TEST(Clause, can_compare_for_equality) {
  std::unordered_set<Lit> cls({Lit(0, true), Lit(1, false), Lit(2, true)});

  Clause c1(cls.begin(), cls.end());
  Clause c2(cls);

  ASSERT_EQ(c1, c2);
}

TEST(Problem, can_parse_simple_files) {
  FORALL_TESTFILES(tfile) {
    Problem p(*tfile);
    ASSERT_GT(p.nVars, 0);
  }
}

TEST(Problem, can_add_clause) {
  Problem p(TEST_FILES[TF_BORDERLINE]);

  std::unordered_set<Lit> lits;
  for (auto tvar : p.target) {
    lits.insert(Lit(tvar, false));
  }
  size_t old = p.clauses.size();
  p.add_clause(lits);
  ASSERT_GT(p.clauses.size(), old);
}
