/* 
 * BaxMC - Copyright (C) - UGA - CNRS - Grenoble-INP
 *
 * By
 *
 * Thomas Vigouroux
 * Cristian Ene
 * David Monniaux
 * Laurent Mounier
 * Marie-Laure Potet
 *
 * This software is a computer program whose purpose is to, given a boolean formula as well as the
 * specific role each of the variables appearing in the formula should be considered, find the
 * assignement that maximizes the number of models of the formula over some set of variables.
 *
 * This software is governed by the CeCILL-B license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/ or redistribute the software under the
 * terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute
 * granted by the license, users are provided only with a limited warranty and the software's
 * author, the holder of the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using,
 * modifying and/or developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the software's suitability as
 * regards their requirements in conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-B
 * license and that you accept its terms.
 */

#include "baxmc.hpp"
#include "approxmc/approxmc.h"
#include "defs.hpp"
#include <fstream>
#include <gtest/gtest.h>
#include <stddef.h>

using namespace BaxMC;
using namespace CMSat;
using namespace ApproxMC;
using std::unordered_set;

// Test that all cases match the reality
TEST(BaxMC, borderline) {
  Problem p(TEST_FILES[TF_BORDERLINE]);
  BMC solver(p);

  // For borderline the solution is all 1
  unordered_set<Lit> expected;
  for (auto var : p.target) {
    expected.insert(Lit(var, false));
  }
  auto res = solver.solve();
  ASSERT_EQ(0, res.first.hashCount);
  ASSERT_EQ(256, res.first.cellSolCount);
  ASSERT_EQ(expected, res.second);
}

TEST(BaxMC, linear) {
  Problem p(TEST_FILES[TF_LINEAR]);
  BMC solver(p);

  // For linear the solution is all 0
  unordered_set<Lit> expected;
  for (auto var : p.target) {
    expected.insert(Lit(var, true));
  }

  auto res = solver.solve();
  ASSERT_EQ(23, res.first.hashCount);
  ASSERT_EQ(512, res.first.cellSolCount);
  ASSERT_EQ(expected, res.second);
}

TEST(BaxMC, modular) {
  Problem p(TEST_FILES[TF_MODULAR]);
  BMC solver(p);

  // Solution is 2, -3, -4, 5, -6, -7, -8, -9
  unordered_set<Lit> expected({
      Lit(1, false),
      Lit(2, true),
      Lit(3, true),
      Lit(4, false),
      Lit(5, true),
      Lit(6, true),
      Lit(7, true),
      Lit(8, true),
  });

  auto res = solver.solve();
  ASSERT_EQ(0, res.first.hashCount);
  ASSERT_EQ(28, res.first.cellSolCount);
  ASSERT_EQ(expected, res.second);
}
